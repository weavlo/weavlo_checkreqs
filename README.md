# Weavlo Requirement Checker
This is the 1st program in the [Weavlo](https://gitlab.com/weavlo) software suite.

## Purpose
We need a program to *check* if all ERPNext PreRequisites are installed.

Note: This tool will **not** install software itself.  That is not its purpose.  It's should only Check/Validate existence of other software.

## Implementation
The system we're checking may not have Python.
We want this program to run anyway, and return that information to the use.
Ergo, this program should not be written in Pthon.  It should be a standalone, binary executable with no dependencies of its own.

## Potential PreRequisites to check.
This list is a Topic for discussion.

  * Supported Operating Systems (Debian, Ubuntu, CentOS, Fedora, MacOS, etc.)
  * Python programming language
    * Python Pip
  * MariaDB database server.
    * (we're going to try eliminating MariaDB *Client* as a equirement for installing ERPNext)
  * Nginx (Production Only)
    * Question: How will we *know* whether we're checking for Production Requirements, or not?  Input parameter?
  * Nodejs
    * Node Package Manager
    * (yarn will not longer be required; it's redundant)
  * Redis
  * wkhtmltopdf. Converts HTML pages to PDF documents.

## [bool,json] = func_WeavloCheckPrereqs(str  _semanticversion)

### Inputs
The semantic version of ERPNext we are checking.

Requirements can change over time.  ERPNext may have different pre-requisites for Version 10, 11, 12, etc.  This program needs to know what requirements to check against.
  
 ### Outputs
   * A boolean True/False, indicating all pre-requisites are available.
   * A JSON/TOML file that contains:
     * Name of PreReq Software found by the tool.
     * Version(s) found
     * Path(s) found
     * If software *not* found, indicate that too.
 
 ## Questions To Answer
   * How will it know what to look for?  A file?  Ansible playbooks?
   * What **must be installed manually**, by a User, before running the Weavly Installer for ERPNext?
   * What will we **optionally** offer to install for the user, to make things easier on them.
   * This absolutely must be a CLI tool.
   * We also want this to Page 1 of the Weavly WebUI Installer.  The moment you download the installer and click "run", these prerequisites should be checked, and results displayed on the screen to the user.
